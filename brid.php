<?php
/**
 * @copyright 	Copyright (c) 2012-2017 Brid Video DOO (https://www.brid.tv)
 *
 * This file is part of Brid.tv project.
 *
 * FP Instant articles iframe generator.
 *
 * @version  	1.0
 * @since 		1.0
 * @author  	BridTv Dev. Team <support@brid.tv>
 * This script can not be copied and/or distributed without the express.
 */
try{
	/**
	 * Allowed parameteres sent via GET
	 */
	$allowed = [
					'id',
					'video',
					'autoplay',
					'width',
					'height',
					'title',
					'playButtonColor',
					'titleBar',
					'slide_inview',
					'monetize',
					'autoplay_desktop',
					'autoplay_on_ad',
					'autoplayInview',
					'slide_inview_show',
					'slide_inview_seconds',
					'share',
					'volume',
					'fullscreen',
					'controls',
					'controlsBehavior',
					'videoPlayback',
					'playlistPlayback',
					'afterPlaylistEnd',
					'voipSocEnabled',
					'comscore',
					'veepsChat',
					'pauseOffView',
					'start_volume',
					'hover_volume',
					'play_in_page',
					'inpage_mobile',
					'playlistScreen',
					'shareScreen',
					'primaryColor',
					'adSDK',
					'partner_id',
					'Ad'
				];

	/**
	  * Required parameters
	  */
	$required = ['id', 'video', 'partner_id'];
	/**
	  * Page title
	  */
	$pageTitle = '';
	/**
	  * Brid unique id
	  */
	$unique_id = 'Brid_'.substr(md5(time()), 6);
	/**
	  * Parnter id
	  */
	$partner_id = null;
	/**
	 * Default options array
	 */
	$options = ['id'=>null, 'video'=>null, 'width'=>'100%', 'height'=>'100%'];
	if(isset($_GET)){
		$options = array_merge($options, $_GET);
	}
	//Sanitize
	foreach($options as $k=>$v){
		if(!in_array($k, $allowed)){
			unset($options[$k]);
		}
	}
	//Array values
	foreach($options as $k=>$v){
		if(in_array($k, ["adSDK", "Ad"])){
			//unset($options[$k]);
			$options[$k] = json_decode($options[$k]);
		}
	}
	//unset title from options
	if(isset($options['title'])){
		$pageTitle = strip_tags($options['title']);
		unset($options['title']);
	}

	//Check required
	foreach($required as $k=>$v){
		if(!isset($options[$v])){
			throw new Exception('Required parameter: "'.$v.'" is missing.');
		}
	}
	
	//unset pid from options
	if(isset($options['partner_id'])){
		$partner_id = intval($options['partner_id']);
		unset($options['partner_id']);
	}

?>
<html lang=en style="height:100%">
<head>
<title><?php echo $pageTitle; ?></title>
<script type="text/javascript" src="//services.brid.tv/player/build/brid.min.js"></script>
<meta name=viewport content="width=device-width,initial-scale=1,user-scalable=no" />
<meta itemprop="name" content="<?php echo $pageTitle; ?>">
<meta itemprop="thumbnailUrl" content="//cdn.brid.tv/live/partners/<?php echo $partner_id; ?>/snapshot/<?php echo $options['video']; ?>/00002.png">
<meta itemprop="contentURL" content="//cdn.brid.tv/live/partners/<?php echo $partner_id; ?>/sd/<?php echo $options['video']; ?>.mp4">
<meta itemprop="embedURL" content="//services.brid.tv/services/iframe/video/<?php echo $options['video']; ?>/<?php echo $partner_id; ?>">
</head>
<body style="margin:0px;padding:0px;height:100%;overflow:hidden;">
		<div id="<?php echo $unique_id; ?>" class="brid" itemprop="video" itemscope itemtype="http://schema.org/VideoObject"></div>
		<script type="text/javascript">$bp("<?php echo $unique_id; ?>",<?php echo json_encode($options); ?>);</script>
</body>
</html>
<?php
}catch(Exception $e){
	//handle exception
	echo $e->getMessage();
}
?>